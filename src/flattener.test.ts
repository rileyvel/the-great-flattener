import {Flattener} from "./flattener";

describe('Flattener', () => {

    it('should flatten correctly', () => {
        const f = new Flattener(testObj);
        const outcome = f.flatten();
        expect(JSON.stringify(outcome)).toEqual(JSON.stringify(expectedOutcome));
    });

    it('should return a flat object as is', () => {
        const f = new Flattener({});
        const outcome = f.flatten();
        expect(JSON.stringify(outcome)).toBe('{}');
    });

});

const testObj = {
    data: 12345,
    str: "Hello",
    meow: {
        meow: {
            str2: "World!",
            woof: {
                str3: "I'm",
                quack: {
                    str3: "quokka"
                },
                duck: [
                    {
                        str4: 'qqp',
                        str5: 'bbb'
                    },
                    {
                        obj: {
                            str6: 'tahd',
                            str7: 'aoeuidhtns'
                        }
                    }
                ]
            }
        }
    },
    str4: "here"
};

const expectedOutcome = {
    "data": 12345,
    "str": "Hello",
    "str2": "World!",
    "str3": "I'm",
    "str3Copy": "quokka",
    "duck": [{"str4": "qqp", "str5": "bbb"}, {"str6": "tahd", "str7": "aoeuidhtns"}],
    "str4": "here"
};